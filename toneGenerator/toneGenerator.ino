
/*
 * circut
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * the speaker is in series from 9 -> speaker -> switch -> pot -> ground
 */
 

int speaker = 9;    // LED connected to digital pin 9
int sensor = A0;
void setup()  { 
  // nothing happens in setup
} 

void loop()  { 
    int sensorValue = analogRead(sensor);
    analogWrite(speaker,sensorValue);
}


